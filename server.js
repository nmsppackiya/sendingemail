const express = require('express');
const nodemailer = require('nodemailer');
const smtpTransport = require('nodemailer-smtp-transport');
const app = express();

const env = {
	SMTP_HOST: 'mail.XXX.com',
	SMTP_PORT: 587,
	SMTP_AUTH_USER: 'tester@XXX.com',
	SMTP_AUTH_PASS: 'XXXX',
	SMTP_FROM: 'XXX'
};

const transporter = nodemailer.createTransport(smtpTransport({
	host: env.SMTP_HOST,
	port: env.SMTP_PORT,
	tls: {
		rejectUnauthorized: false
	},
	auth: {
		user: env.SMTP_AUTH_USER,
		pass: env.SMTP_AUTH_PASS
	}
}));


app.get('/',(req,res)=>{
	res.send('WORKING');
});

app.get('/send-email',(req,res) => {
	var msg = {
		html: "<b>Its working!!!!</b><p>Welcome</p>.",
		createTextFromHtml: true,
		from: env.SMTP_FROM,
		to: "XXXX@gmail.com",
		subject: "Checking Mail"
	};
	transporter.sendMail(msg, function(error, response){
		if(error){
			console.log(error);
		}else{
			console.log("Message sent: " + response.envelope);
			res.send(response);
		}

		transporter.close();
	});
});


app.listen(8000,()=>{
	console.log("Server is running on the Port: 8000");
})
